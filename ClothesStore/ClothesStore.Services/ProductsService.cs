﻿using ClothesStore.Database;
using ClothesStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothesStore.Services
{
    public class ProductsService
    {
        public Product GetProduct(int id)
        {
            using (var context = new CSContext())
            {
                return context.Products.Find(id);
            }
        }
        public List<Product> GetProducts()
        {
            using (var context = new CSContext())
            {
                return context.Products.ToList();
            }
        }

        public void SaveProduct(Product product)
        {
            using (var context = new CSContext())
            {
                context.Products.Add(product);
                context.SaveChanges();
            }
        }

        public void UpdateProduct(Product product)
        {
            using (var context = new CSContext())
            {
                context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteProduct(int Id)
        {
            using (var context = new CSContext())
            {
                var product = context.Products.Find(Id);
                //context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                context.Products.Remove(product);
                context.SaveChanges();
            }
        }
    }
}
