﻿using ClothesStore.Database;
using ClothesStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothesStore.Services
{
    public class CategoriesService
    {
        public Category GetCategory(int id)
        {
            using (var context = new CSContext())
            {
                return context.Categories.Find(id);
            }
        }
        public List<Category> GetCategories()
        {
            using (var context = new CSContext())
            {
                return context.Categories.ToList();
            }
        }

        public void SaveCategory(Category category)
        {
            using (var context = new CSContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }

        public void UpdateCategory(Category category)
        {
            using (var context = new CSContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteCategory(int Id)
        {
            using (var context = new CSContext())
            {
                var category = context.Categories.Find(Id);
                //context.Entry(category).State = System.Data.Entity.EntityState.Deleted;
                context.Categories.Remove(category);
                context.SaveChanges();
            }
        }
    }
}
